__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'

from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('main.views',
    # Examples:
    # url(r'^$', 'pcservice.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', 'home', name='home'),
    url(r'^price', 'price', name='price'),
    url(r'^repairpc', 'repair_pc', name='repair_pc'),
)
